FROM rifqi669/rpi-alpine-cvruby:builder as opencv
FROM rifqi669/rpi-alpine-ruby:latest as builder
COPY --from=opencv /usr/local /usr/local
RUN apk --update --no-cache add \
        build-base \
        cmake \
        python3 \
        linux-headers \
        alpine-sdk && \
    ln -sf python3 /usr/bin/python && \
    gem install -V ropencv

# final image

FROM rifqi669/rpi-alpine-ruby:latest
COPY --from=builder /usr/local /usr/local
RUN apk --no-cache add \
        ffmpeg \
        tiff \
        openjpeg

CMD [ "irb" ]
