FROM balenalib/rpi-alpine:latest

ENV OPENCV_VERSION=4.5.3

RUN apk --update --no-cache --virtual builder add \
        build-base \
        python3 \
        bash \
        ca-certificates \
        cmake \
        coreutils \
        curl \
        freetype-dev \
        ffmpeg-dev \
        ffmpeg-libs \
        gcc \
        g++ \
        git \
        gettext \
        lcms2-dev \
        libavc1394-dev \
        libc-dev \
        libffi-dev \
        libjpeg-turbo-dev \
        libpng-dev \
        libressl-dev \
        linux-headers \
        make \
        musl \
        openjpeg-dev \
        openssl \
        tiff-dev \
        unzip \
        zlib-dev \
        wget \
        musl-dev \
        openjpeg-tools \
        v4l-utils-dev \
        pkgconfig \
        alpine-sdk \
        linux-headers && \
    wget https://github.com/opencv/opencv/archive/${OPENCV_VERSION}.zip -O /tmp/opencv-${OPENCV_VERSION}.zip && \
    unzip /tmp/opencv-${OPENCV_VERSION}.zip -d /tmp/ && \
    mkdir /tmp/opencv-${OPENCV_VERSION}/release && \
    cd /tmp/opencv-${OPENCV_VERSION}/release && \
    cmake \
        -D OPENCV_GENERATE_PKGCONFIG=YES \
        -D OPENCV_ENABLE_NONFREE=YES \
        -D WITH_FFMPEG=YES \
        -D WITH_TBB=YES \
        -D WITH_OPENMP=YES \
        -D WITH_V4L=YES \
        -D WITH_LIBV4L=YES \
        -D WITH_QT=NO \
        -D WITH_WEBP=NO \
        -D VIDEOIO_PLUGIN_LIST=all \
        -D ENABLE_VFPV3=NO \
        -D ENABLE_NEON=NO \
        -D ENABLE_LTO=YES \
        -D ENABLE_PRECOMPILED_HEADERS=NO \
        -D INSTALL_PYTHON_EXAMPLES=NO \
        -D INSTALL_C_EXAMPLES=NO \
        -D BUILD_TBB=YES \
        -D BUILD_ANDROID_EXAMPLES=NO \
        -D BUILD_DOCS=NO \
        -D BUILD_TESTS=NO \
        -D BUILD_PERF_TESTS=NO \
        -D BUILD_EXAMPLES=NO \
        -D BUILD_opencv_java=NO \
        -D BUILD_opencv_python=NO \
        -D BUILD_opencv_python2=NO \
        -D BUILD_opencv_python3=NO \
        -D CMAKE_BUILD_TYPE=Release \
        -D CMAKE_INSTALL_PREFIX=/usr/local .. && \
    make -j$(nproc) && \
    make install
